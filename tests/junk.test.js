test('that 1 is 1', () => {
  expect(1).toBe(1);
});

test('that 1 is not 3', () => {
  expect(1).not.toBe(3);
});


test('that 2 is not 3', () => {
  expect(2).not.toBe(3);
});

